var mongoose = require('mongoose')
var Schema = mongoose.Schema

var productSchema = new Schema({
    name: {
        type: String,
        uppercase: true
    },
    category:{
        type: String
    },
    brand:{
        type: String
    },
    quantity: {
        type: Number
    },
    price: Number,
    color: String,
    manuDate: {
        type: Date
    },
    expiryDate: {
        type: Date
    },
    origin: {
        type: String
    },
    status: {
        type: String,
        enum: ['available', 'out of stock'],
        default: 'available'
    },
    image: {
        type: String
    },
    tags: {
        type: [String]
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
},{
    timestamps: true
})


var productModel = mongoose.model('product', productSchema)

module.exports = productModel;