import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { SharedRoutingModule } from './shared.routing';
import { MsgService } from './services/msgService';
import { UploadService } from './services/uploads.service';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    SharedRoutingModule,
  ],
  exports: [
    PageNotFoundComponent,
    HeaderComponent,
    FooterComponent
  ],
  providers: [MsgService, UploadService]
})
export class SharedModule { }
