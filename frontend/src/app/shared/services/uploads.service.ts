import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';



@Injectable()
export class UploadService {
    constructor() {
    }
    upload(data: any, files: any, method, url) {
        // tslint:disable-next-line: deprecation
        return Observable.create((observer) => {

            const formData = new FormData();
            const xhr = new XMLHttpRequest();

            if (files[0]) {

                formData.append('image', files[0], files[0].name);
            }
            // tslint:disable-next-line:forin
            for (const key in data) {
                formData.append(key, data[key]);
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            };

            xhr.open(method, url + '?token=' + localStorage.getItem('token'), true);
            xhr.send(formData);

        });
    }
}
