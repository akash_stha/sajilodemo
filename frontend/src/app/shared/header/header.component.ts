import { Component } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  loggedInUser = false;

  constructor(
    public authService: AuthService,
    public router: Router
  ) {
    this.loggedInUser = this.authService.isLoggedIn();
  }

  checkLogin() {
    return this.authService.isLoggedIn();
  }

  checkAdmin() {
    return this.authService.isAdmin();
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
}
