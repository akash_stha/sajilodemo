export class User {

  name: string;
  username: string;
  password: string;
  address: string;
  email: string;
  phoneNumber: number;
  gender: string;

  constructor(options: any) {
    this.name = options.name || '';
    this.username = options.username || '';
    this.password = options.password || '';
    this.address = options.address || '';
    this.email = options.email || '';
    this.phoneNumber = options.phoneNumber || '';
    this.gender = options.gender || '';
  }

}
