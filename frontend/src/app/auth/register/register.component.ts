import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/msgService';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  submitting = false;
  user;
  constructor(
    public authService: AuthService,
    public msgService: MsgService,
    public router: Router
  ) {
    this.user = new User({});
  }

  ngOnInit() {
  }

  register() {
    this.submitting = true;
    this.authService.register(this.user).subscribe(
      data => {
        this.msgService.showSuccess('Registration Successfull');
        this.router.navigate(['/auth/login']);
      },
      error => {
        this.msgService.showError(error);
      }
    );
    setTimeout(() => {
      this.submitting = false;
    }, 1000);
  }

}
