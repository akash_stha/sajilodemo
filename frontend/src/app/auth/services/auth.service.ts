import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './../models/user.model';
import { environment } from 'src/environments/environment';
import { CheckboxModule } from 'angular-bootstrap-md';

@Injectable()

export class AuthService {

  url;

  constructor(
    public http: HttpClient
  ) {
    this.url = environment.baseUrl;
  }

  getOptions() {
    const option = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
    };
    return option;
  }

  // services are used to make an API to backend server
  login(data: User) {
    // connect db of nepastore
    return this.http.post(this.url + 'auth', data, this.getOptions());
  }

  register(data: User) {
    return this.http.post(this.url + 'auth/register', data, this.getOptions());
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  isLoggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  isAdmin() {
    const check = JSON.parse(localStorage.getItem('user'));
    // console.log('check here is', check.role);
    if (check.role === '1') {
      return true;
    } else {
      return false;
    }
  }
}
