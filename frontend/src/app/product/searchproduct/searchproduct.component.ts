import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msgService';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Product } from '../models/product.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-searchproduct',
  templateUrl: './searchproduct.component.html',
  styleUrls: ['./searchproduct.component.scss']
})
export class SearchProductComponent implements OnInit {

  product;
  imgUrl;
  submitting = false;
  result = false;
  products = [];
  allProducts = [];
  catOnly = [];
  categories = [];
  finalCat = [];
  productNames = [];
  showName = false;
  showMultipleDate = false;

  constructor(
    public msgService: MsgService,
    public router: Router,
    public productService: ProductService
  ) {
    this.product = new Product({});
    this.product.category = '';
    this.product.name = '';
    this.imgUrl = environment.ImgUrl;
  }

  ngOnInit() {
    this.productService.get().subscribe(
      (data: any) => {
        this.allProducts = data;

        this.allProducts.forEach((item) => {
          if (this.catOnly.indexOf(item.category) === -1) {
            this.catOnly.push(item.category);
          }
        });

        this.finalCat =
          this.catOnly
            .filter(item => item);


        this.categories = this.catOnly.filter(item => item);
      },
      err => {
        this.msgService.showError(err);
      }
    );
  }

  cancel() {
    this.router.navigate(['/user']);
  }

  submit() {

    if (!this.product.toDate) {
      this.product.toDate = this.product.fromDate ;
    }

    this.submitting = true;
    this.productService.search(this.product).subscribe(
      (data: any) => {
        this.submitting = false;
        console.log('search result is ', data);
        if (data.length) {
          this.result = true;
          this.products = data;
        } else {
          this.msgService.showInfo('No Product Matched')
        }
      }, error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    );
  }

  categorySelected(val) {
    // console.log('value is ',val);

    this.showName = true;

    this.productNames = this.allProducts.filter((item) => {
      if ( item.category === val ) {
        return item;
      }
    });

  }

  searchAgain() {
    this.result = false;
    this.product = new Product({});
    this.showName = false;
    this.product.category = '';
  }

  showMutiple() {
    this.showMultipleDate = !this.showMultipleDate ;

    if (!this.showMultipleDate) {
      this.product.toDate = null;
    }

  }

}
