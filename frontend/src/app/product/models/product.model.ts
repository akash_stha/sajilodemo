export class Product {
  _id: string;
  name: string;
  category: string;
  description: string;
  color: string;
  price: number;
  manuDate: number;
  expirtyDate: any;
  origin: string;
  tags: any;
  modelNo: number;
  quality: string;
  brand: string;
  targetedGroup: string;
  status: string;
  image: string;
  quantity: number;
  model: string;

  constructor(obj: any) {
      for (let key in obj) {
          this[key] = obj[key] || '';
      }
  }


}
