import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.model';
import { MsgService } from 'src/app/shared/services/msgService';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { environment } from 'src/environments/environment';
import { UploadService } from 'src/app/shared/services/uploads.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
export class AddProductComponent implements OnInit {
  product;
  submitting = false;
  filesToUpload = [];
  url;

  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router,
    public uploadService: UploadService
  ) {
    this.url = environment.baseUrl;
  }

  ngOnInit() {
    this.product = new Product({});
  }

  submit() {
    this.submitting = true;
    console.log('this.prouct', this.product);
    this.uploadService.upload(this.product, this.filesToUpload, 'POST', `${this.url}product`).subscribe(
      data => {
        this.submitting = false;
        this.msgService.showSuccess('Product Added Successfully');
        this.router.navigate(['/product/list']);
      }, error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    );

  }
  fileChanged(ev) {
    console.log('files ', ev);
    this.filesToUpload = ev.target.files;
  }

}
