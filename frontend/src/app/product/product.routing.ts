import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddProductComponent } from './addproduct/addproduct.component';
import { EditProductComponent } from './editproduct/editproduct.component';
import { ListProductComponent } from './listproduct/listproduct.component';
import { SearchProductComponent } from './searchproduct/searchproduct.component';


const productRoute: Routes = [
  {
    path: 'add',
    component: AddProductComponent
  },
  {
    path: 'edit/:id',
    component: EditProductComponent
  },
  {
    path: 'list',
    component: ListProductComponent
  },
  {
    path: 'search',
    component: SearchProductComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(productRoute)
  ],
  exports: [RouterModule]
})


export class ProductRoutingModule {

}
