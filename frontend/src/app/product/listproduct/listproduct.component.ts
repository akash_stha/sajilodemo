import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msgService';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';
import { Product } from '../models/product.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.scss']
})
export class ListProductComponent implements OnInit {
  products: Array<Product>;
  loading = false;
  imgUrl;
  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router
  ) {
    this.imgUrl = environment.ImgUrl;
    this.loading = true;
  }

  ngOnInit() {
    this.productService.get().subscribe(
      (data: any) => {
        console.log('data from server', data);
        this.loading = false;
        this.products = data;
      },
      error => {
        this.loading = false;
        this.msgService.showError(error);
      }
    );
  }

  edit(id) {
    this.router.navigate(['product/edit/' + id]);
  }

  remove(id, index) {

    const con = confirm('Are you sure to delete?');
    if (con) {
      this.productService.delete(id).subscribe(
        data => {
          this.msgService.showInfo('Product deleted');
          this.products.splice(index, 1);

        }, err => {
          this.msgService.showError(err);
        }
      );
    }
  }

}
