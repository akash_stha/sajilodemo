import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListProductComponent } from './listproduct/listproduct.component';
import { SharedModule } from '../shared/shared.module';
import { ProductRoutingModule } from './product.routing';
import { FormsModule } from '@angular/forms';
import { AddProductComponent } from './addproduct/addproduct.component';
import { SearchProductComponent } from './searchproduct/searchproduct.component';
import { EditProductComponent } from './editproduct/editproduct.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductService } from './services/product.service';
import { DeleteproductComponent } from './deleteproduct/deleteproduct.component';

@NgModule({
  declarations: [ListProductComponent, AddProductComponent, SearchProductComponent, EditProductComponent, DeleteproductComponent ],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ProductService]
})
export class ProductModule { }
