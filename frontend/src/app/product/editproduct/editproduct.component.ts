import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product.service';
import { MsgService } from 'src/app/shared/services/msgService';
import { Product } from '../models/product.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-editproduct',
  templateUrl: './editproduct.component.html',
  styleUrls: ['./editproduct.component.scss']
})
export class EditProductComponent implements OnInit {
  productId;
  product;
  loading = false;
  submitting = false;
  filesToUpload = [];
  imgUrl;
  constructor(
    public router: Router,
    public productService: ProductService,
    public msgService: MsgService,
    public activeRoute: ActivatedRoute
  ) {
    this.productId = this.activeRoute.snapshot.params['id'];
    this.loading = true;
    this.imgUrl = environment.ImgUrl;
  }

  ngOnInit() {
    this.productService.getById(this.productId).subscribe(
      (data: any) => {
        this.loading = false;
        this.product = data;
        if (data.tags) {
          this.product.tags = data.tags.toString();
        }
        console.log('this product', this.product);
      }, err => {
        this.loading = false;

        this.msgService.showError(err);
      }
    );
  }

  submit() {
    console.log('submit here');
    this.submitting = true;
    this.productService.upload(this.product, this.filesToUpload, 'PUT').subscribe(
      data => {
        this.submitting = false;
        this.msgService.showInfo('Product Updated Successfully');
        this.router.navigate(['/product/list']);
      }, error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    );
  }
  changeFile(ev) {
    this.filesToUpload = ev.target.files;
  }

}
