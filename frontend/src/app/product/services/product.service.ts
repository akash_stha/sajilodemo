import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ProductService {
    url;
    constructor(public http: HttpClient) {
        this.url = environment.baseUrl;
    }

    get() {
        return this.http.get(this.url + 'product', this.getOptionsWithToken());
    }
    getById(id: string) {
        return this.http.get(this.url + 'product/' + id, this.getOptionsWithToken());

    }
    add(data: Product) {
        return this.http.post(this.url + 'product', data, this.getOptionsWithToken());

    }
    update(data: Product) {
        return this.http.put(this.url + 'product/' + data._id, data, this.getOptionsWithToken());

    }
    delete(id: string) {
        return this.http.delete(this.url + 'product/' + id, this.getOptionsWithToken());

    }
    search(data: Product) {
        return this.http.post(this.url + 'product/search', data, this.getOptionsWithToken());

    }
    upload(data: Product, files: any, method) {
        // tslint:disable-next-line: deprecation
        return Observable.create((observer) => {

            const formData = new FormData();
            const xhr = new XMLHttpRequest();

            if (files[0]) {

                formData.append('image', files[0], files[0].name);
            }
            // tslint:disable-next-line: forin
            for (const key in data) {
                formData.append(key, data[key]);
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            let url;
            const token = localStorage.getItem('token');
            if (method === 'POST') {
                url = this.url + 'product?token=' + token;
            } else {
                url = `${this.url}product/${data._id}?token=${token}`;
            }
            xhr.open(method, url, true);
            xhr.send(formData);

        });

    }

    getOptionsWithToken() {
        const option = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('token')
            })
        };
        return option;
    }


}
